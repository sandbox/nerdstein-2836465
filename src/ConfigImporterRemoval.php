<?php

namespace Drupal\config_removal;

use Drupal\Core\Config\ConfigImporter;
use Drupal\Core\Config\StorageComparer;

/**
 * Class ConfigImporterRemoval.
 *
 * @package Drupal\config_removal
 */
class ConfigImporterRemoval {

  /**
   * Replace the overridden values with the original ones.
   *
   * @param array $context
   *   Context of the config import.
   * @param \Drupal\Core\Config\ConfigImporter $config_importer
   *   Config importer object.
   */
  public static function postImport(array &$context, ConfigImporter $config_importer) {
    // Load all config removal entities.
    $crs = \Drupal::entityTypeManager()->getStorage('config_removal')->loadMultiple();
    $deleted = [];
    /** @var \Drupal\config_removal\Entity\ConfigRemoval $cr */
    foreach ($crs as $cr) {
      $removal = $cr->getRemovalList();

      // Check for one element.
      if (!empty($removal) and !is_array($removal)) {
        $removal = [$removal];
      }

      foreach ($removal as $remove) {
        /** @var \Drupal\Core\Config\Config $removed_config */
        $removed_config = \Drupal::service('config.factory')->getEditable($remove);
        if ($removed_config) {
          $removed_config->delete();
          $deleted[] = $remove;
        }
      }
    }

    $context['finished'] = 1;

    if (!empty($deleted)) {

      // The list of names looks different depending on output medium.
      // If terminal (CLI), then no markup.
      if (php_sapi_name() == 'cli' || isset($_SERVER['argc']) && is_numeric($_SERVER['argc'] && $_SERVER['argc'] > 0)) {
        $names_list = "\n\r  " . implode("\n\r  ", $deleted);
      }
      else {
        $output = [
          '#theme' => 'item_list',
          '#list_type' => 'ul',
          '#items' => $deleted,
        ];
        $names_list = render($output);
      }

      // `PluralTranslatableMarkup` does not seem to handle HTML as well as
      // plain t() does. It will not allow the <ul> list in the browser, and
      // renders the lists HTML as clear text.
      if (count($deleted) == 1) {
        $message = t('The following config was removed: @list', ['@list' => $names_list]);
      }
      else {
        $message = t('The following @count config were removed: @list', ['@count' => count($deleted), '@list' => $names_list]);
      }

      drupal_set_message($message, 'warning');
    }
  }

  /**
   * Match a config entity name against the list of ignored config entities.
   *
   * @param \Drupal\Core\Config\StorageComparer $storage_comparer
   *   Storage Comparer object.
   * @param string $config_name
   *   The name of the config entity to match against all ignored entities.
   *
   * @return bool
   *   True, if the config entity is to be ignored, false otherwise.
   */
  public static function matchConfigName(StorageComparer $storage_comparer, $config_name) {
    // Gather all config should be removed from the active config.
    $removed_config = [];

    // Load all config removal entities.
    $crs = \Drupal::entityTypeManager()->getStorage('config_removal')->loadMultiple();
    /** @var \Drupal\config_removal\Entity\ConfigRemoval $cr */
    foreach ($crs as $cr) {
      $removal = $cr->getRemovalList();
      if (in_array($config_name, $removal)) {
        return TRUE;
      }
    }

    return FALSE;
  }

}
