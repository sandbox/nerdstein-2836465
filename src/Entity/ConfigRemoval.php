<?php

namespace Drupal\config_removal\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the Configuration Removal entity.
 *
 * @ConfigEntityType(
 *   id = "config_removal",
 *   label = @Translation("Configuration Removal"),
 *   handlers = {
 *     "list_builder" = "Drupal\config_removal\ConfigRemovalListBuilder",
 *     "form" = {
 *       "add" = "Drupal\config_removal\Form\ConfigRemovalForm",
 *       "edit" = "Drupal\config_removal\Form\ConfigRemovalForm",
 *       "delete" = "Drupal\config_removal\Form\ConfigRemovalDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\config_removal\ConfigRemovalHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "config_removal",
 *   admin_permission = "administer configuration removal",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "add-form" = "/admin/config/development/configuration/config-removal/add",
 *     "edit-form" = "/admin/config/development/configuration/config-removal/{config_removal}/edit",
 *     "delete-form" = "/admin/config/development/configuration/config-removal/{config_removal}/delete",
 *     "collection" = "/admin/config/development/configuration/config-removal"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "removal_list",
 *   }
 * )
 */
class ConfigRemoval extends ConfigEntityBase implements ConfigRemovalInterface {

  /**
   * The Configuration Split Setting ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Configuration Split Setting label.
   *
   * @var string
   */
  protected $label;

  /**
   * The explicit configuration to remove.
   *
   * @var string[]
   */
  protected $removal_list = [];

  /**
   * {@inheritdoc}
   */
  public function getRemovalList() {
    return $this->removal_list;
  }

}
