<?php

namespace Drupal\config_removal\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Configuration Removal entities.
 */
interface ConfigRemovalInterface extends ConfigEntityInterface {

  /**
   * Returns the list of config to remove.
   *
   * @return mixed
   *   Array of configuration keys.
   */
  public function getRemovalList();

}
