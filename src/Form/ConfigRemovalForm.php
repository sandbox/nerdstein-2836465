<?php

namespace Drupal\config_removal\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Class ConfigRemovalForm.
 *
 * @package Drupal\config_removal\Form
 */
class ConfigRemovalForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    /** @var \Drupal\config_removal\Entity\ConfigRemovalInterface $config */
    $config = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $config->label(),
      '#description' => $this->t("Label"),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $config->id(),
      '#machine_name' => [
        'exists' => '\Drupal\config_removal\Entity\ConfigRemoval::load',
      ],
      '#disabled' => !$config->isNew(),
    ];

    $form['removal_list'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Removal List'),
      '#description' => $this->t('One configuration key per line.'),
      '#default_value' => implode("\n", $config->getRemovalList()),
      '#required' => TRUE,
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $list = explode("\n", $form_state->getValue('removal_list'));
    for ($i = 0; $i < count($list); $i++) {
      $list[$i] = trim($list[$i]);
    }
    $form_state->setValue('removal_list', $list);
    parent::submitForm($form, $form_state);
  }

  /**
   * Filter text input for valid configuration names (including wildcards).
   *
   * @param string|string[] $text
   *   The configuration names, one name per line.
   *
   * @return string[]
   *   The array of configuration names.
   */
  protected function filterConfigNames($text) {
    if (!is_array($text)) {
      $text = explode("\n", $text);
    }
    // Filter out illegal characters.
    return array_filter(preg_replace('/[^a-z0-9\._\*]+/', '', $text));
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $config_removal = $this->entity;
    $status = $config_removal->save();

    switch ($status) {
      case SAVED_NEW:
        drupal_set_message($this->t('Created the %label removal configuration.', [
          '%label' => $config_removal->label(),
        ]));
        break;

      default:
        drupal_set_message($this->t('Saved the %label removal configuration.', [
          '%label' => $config_removal->label(),
        ]));
    }
    $form_state->setRedirectUrl(new Url('entity.config_removal.collection'));
  }

}
